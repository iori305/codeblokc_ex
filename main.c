#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main()
{

#if 0
    char c;
    char s[100] = {};
    int i = 0;

    while(1)
    {
        if ( ( c = getchar() ) != EOF )
            s[i++] = c;

        if ( c == '\n' )
        {
            printf("%s",s);
            memset(s, 0, sizeof(s));
            i = 0;
        }
    }

#else

//    int c, i;
    int i = 0;
    char c;
//    char s[100];
    char s[100] = {};
//    for(i = 0; i < 100; i++)
//        s[i] = 0;

    while(1)
    {   // 1. 將 for loop 換成while
//    for(i = 0; (c = getchar()) != EOF; i++)
//    {
        if( ( c = getchar()) != '\n')
        {   // 2. EOF應該是檔案結尾,
            // console中比較會碰到的是按下enter時會出現的\n
            // 所以一但遇到\n就要去列印輸入結果
            // 否則就繼續儲存到陣列中
            s[i++] = c;
            //putchar(c);
        }
        else
        {   // 此時, 判斷到按下enter的字元'\n'
//            s[i++] = c;
            s[i++] = c;
            // 列印陣列s
            printf("%s",s);

            //清除陣列s
//            for(i = 0; i < 100; i++)
//                s[i] = 0;
            memset(s, 0, sizeof(s));
            // 清除i
            i = 0;
        }
    }

#endif
    return 0;
}

void TimesTable(void)
{
    int i, j;

    for ( i = 1; i<= 9; i++)
    {
        for ( j = 2; j<= 5; j++)
        {
            printf("%d x %d = %d  ", j, i, j*i);
            if ( ( j*i ) < 10 )
            {
                printf(" ");
            }
        }
        printf("\n");
    }

    printf("\n");

    for ( i = 1; i<= 9; i++)
    {
        for ( j = 6; j<= 9; j++)
        {
            printf("%d x %d = %d  ", j, i, j*i);
            if ( ( j*i ) < 10 )
            {
                printf(" ");
            }
        }
        printf("\n");
    }
}
